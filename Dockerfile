FROM airhacks/wildfly
RUN chgrp -R 0 ${WILDFLY_HOME}
RUN chmod -R g+rw ${WILDFLY_HOME}
RUN find ${WILDFLY_HOME} -type d -exec chmod g+x {} +
COPY ./target/hello.war ${DEPLOYMENT_DIR}
